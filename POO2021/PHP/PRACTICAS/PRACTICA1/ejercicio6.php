<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<!<!-- David Sanchez Cayon 29/06/2021 -->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 6</title>
    </head>
    <body>
        <?php
            echo "Quiero que coloque este texto en pantalla" . '<p align="center"';
            echo "Academia Alpe" ,"</p>";
        ?>
        <h2> Puedo colocar todo en el mismo echo</h2>
        
        <?php
            echo 'Quiero que coloque este texto en pantalla<p align="center">Academia Alpe</p>';
        ?>
    </body>
</html>
