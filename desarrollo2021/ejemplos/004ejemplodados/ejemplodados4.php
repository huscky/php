<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            #tabla{
                display:table;
                border-collapse: separate;
                border-spacing: 10px;
            }
            #tabla>div{
                display:table-cell;
                width:50px;
                height: 50px;
                text-align: center;
                vertical-align: middle;
                font-size: 2em;
                color:#ccc;
                border: 1px solid black;
            }
            
            #ficha{
                background-image: url('./imgs/circle.svg');
                width:50px;
                height: 50px;
            }
        </style>
    </head>
    <body>
        <?php
        $numero1= mt_rand(1,6);
        $numero2= mt_rand(1,6); 
        $suma= $numero1+$numero2; 
        ?>
        <img src="./imgs/<?= $numero1?>.svg" alt="alt"/>
        <img src="./imgs/<?= $numero2?>.svg" alt="alt"/>
        <div id="tabla">
        <?php
        
        for($c=0;$c<=12;$c++){
            
        ?>
        
        <div>
        <?php
            if($c==($suma)){
        ?>
        <img id="ficha" src="./imgs/circle.svg">
        <?php
            }
            echo $c;
        ?>
        </div>
        <?php
        
        }
        ?>
        </div>
        
        
        
        
    </body>
</html>
