<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 2 de la Hoja de Ejercicios 1 de PHP</title>
    </head>
    <body>
    <table width=100%" border="1">   
        <tr>
            <td>
                <?php
                //cuando utiliceis la instrucción echo puedes utilizar las comillas dobles o simples.
                echo 'Este texto quiero que lo escribas utilizando la función echo de php';
                ?>
            </td>
            <td>Aquí debe colocar un texto directamente en HTML</td>
        </tr>
        <tr>
            <td>
                <?php
                //Cuando utiliceis la instrucción print puedes utilizar las comillas dobles o simples.
                print 'Este texto quiero qu elo escribas utilizando la funcion print de php';
                ?>
            </td>
            <td>
                <?php
                echo "Academia Alpe";
                ?> 
            </td>
        </tr>
                
               
    </table>
    </body>
</html>
